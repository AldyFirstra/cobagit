import React, { createContext, useState } from "react";
import axios from "axios";

export const GlobalContext = createContext();

export const GlobalProvider = (props) => {
  // initial state yang akan dikirim
  const [data, setData] = useState(null);
  const [fectData, setFectData] = useState(true);

  // current id untuk update
  const [currentId, setCurrentId] = useState(-1);

  //create data & handle input
  const [input, setInput] = useState({
    name: "",
  });

  let state = {
    data,
    fectData,
    setFectData,
    input,
  };



  //   intial function yang akan dikirim
  // handler untuk inputan
  function handleInput(event) {
    let name = event.target.name;
    let value = event.target.value;

    if (name === "name") {
      setInput({ ...input, name: value });
    }
  }

  // handler untuk submit
  const handleSubmit = (event) => {
    event.preventDefault();

    let { name } = input;

    if (currentId === -1) {
      axios
        .post("https://backendexample.sanbercloud.com/api/contestants", {
          name,
        })
        .then((res) => {
          console.log(res);
          setFectData(true);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      let id = currentId;
      axios
        .put(`https://backendexample.sanbercloud.com/api/contestants/${id}`, {
          name,
        })
        .then((res) => {
          setFectData(true);
          setCurrentId(-1);
        })
        .catch((err) => {});
    }

    setInput({ name: "" });
  };

  // handler delete
  const handleDelete = (event) => {
    let id = parseInt(event.target.value);

    axios
      .delete(`https://backendexample.sanbercloud.com/api/contestants/${id}`)
      .then((res) => {
        setFectData(true);
      })
      .catch((err) => {});
  };

  // handler edit
  const handleEdit = (event) => {
    let id = parseInt(event.target.value);
    setCurrentId(id);

    axios
      .get(`https://backendexample.sanbercloud.com/api/contestants/${id}`)
      .then((res) => {
        let data = res.data;
        setInput({ ...input, name: data.name });
      })
      .catch((err) => {});
  };

//   handler fectdata
const handleFectData = () =>{
    axios
    .get(" https://backendexample.sanbercloud.com/api/contestants")
    .then((res) => {
      // let dAta = res.data

      // let resultData = dAta.map((e)=>{
      //   let {
      //     name,
      //     id,
      //   } = e;

      //   return{
      //     name,
      //     id,
      //   }
      // })
      // setData(resultData)

      setData([...res.data]);
      setFectData(false);
    })
    .catch((err) => {});
}

  let handle = {handleDelete,handleEdit,handleInput,handleSubmit, handleFectData}

  return (
    <GlobalContext.Provider value={{ state, handle }}>
      {props.children}
    </GlobalContext.Provider>
  );
};
