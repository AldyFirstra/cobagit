import React, { useEffect, useState } from "react";
import axios from "axios";
import { Table } from "flowbite-react";

const FectDataTable = () => {
  const [dataNilai, setDataNilai] = useState(null);

  useEffect(() => {
    axios
      .get("https://backendexample.sanbercloud.com/api/student-scores")
      .then((res) => {
        let data = res.data;
        let resultData = data.map((result) => {
          let { course, created_at, id, name, score, updated_at } = result;

          return {
            name,
            score,
            id,
            course,
          };
        });

        setDataNilai(resultData);
      })
      .catch((err) => {});
  }, []);

//   console.log(dataNilai);
function handleIndexScore(score){
    if(score >= 80){
        return "A"
    }else if(score >=70 && score <80){
        return "B"
    }else if(score >=60 && score <70){
        return "C"
    }else if(score >=50 && score <60){
        return "D"
    }else if(score >=0 && score <50){
        return "E"
    }else{
        return "Nilai tidak boleh kosong"
    }
}
  return (
    <>
      <div className="w-3/4 mx-auto mt-10">
        <Table>
          <Table.Head className="bg-[#8b5cf6] text-white">
            <Table.HeadCell>No</Table.HeadCell>
            <Table.HeadCell>Nama</Table.HeadCell>
            <Table.HeadCell>Mata Kuliah</Table.HeadCell>
            <Table.HeadCell>Nilai</Table.HeadCell>
            <Table.HeadCell>Index Nilai</Table.HeadCell>
          </Table.Head>
          <Table.Body className="divide-y">
            {dataNilai !== null &&
              dataNilai.map((res,Index) => {
                return (
                  <>
                    <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                      <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                        {Index+1}
                      </Table.Cell>
                      <Table.Cell>{res.name}</Table.Cell>
                      <Table.Cell>{res.course}</Table.Cell>
                      <Table.Cell>{res.score}</Table.Cell>
                      <Table.Cell>{handleIndexScore(res.score)}</Table.Cell>
                    </Table.Row>
                  </>
                );
              })}
          </Table.Body>
        </Table>
      </div>
    </>
  );
};

export default FectDataTable;
