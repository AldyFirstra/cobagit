import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../Context/GlobalContext";

const FectData = () => {

  // initial useContext
  const {state, handle} = useContext(GlobalContext)

  // mengambil state dari globalcontext
  const {data,  fectData, setFectData,  input,} = state

  // const {handle} = useContext(GlobalContext)
  const {handleDelete,handleEdit,handleInput,handleSubmit, handleFectData} = handle

 
  useEffect(() => {
    handleFectData()
  }, [fectData, setFectData]);

 

  // console.log(data);
  return (
    <>
      <h2>Form Data</h2>
      <form onSubmit={handleSubmit}>
        <span>Nama :</span>
        <input
          type={"text"}
          name="name"
          value={input.name}
          onChange={handleInput}
        />
        <input type={"submit"} />
      </form>
      <ol>
        {data !== null &&
          data.map((hsl, Index) => {
            return (
                <li key={Index}>
                  {Index + 1} {hsl.name} | &nbsp;
                  <button value={hsl.id} onClick={handleEdit}>
                    edit
                  </button>
                  <button value={hsl.id} onClick={handleDelete}>
                    delete
                  </button>
                </li>
            );
          })}
      </ol>
    </>
  );
};

export default FectData;
