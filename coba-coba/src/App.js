import React, {useEffect, useState} from 'react';
import axios from 'axios';
import './App.css';
import FectData from './component/fetchData';
import FectDataTable from './component/fectDataTable';
import { GlobalProvider } from './Context/GlobalContext';

function App() {
  return(
    <GlobalProvider>
      <FectData/>
    </GlobalProvider>
  )
}

export default App;
